from django.urls import path, include
from .views import waiter_interface, cook

urlpatterns = [
    path('accounts/waiter/', waiter_interface, name='waiter_interface'),
    path('accounts/cook', cook, name='cook')
]
