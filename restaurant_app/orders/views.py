from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from dishes.models import Dish, DishGroup
from orders.models import DishOrderBinding, Order, Table

@login_required
@user_passes_test(lambda user: user.type == '1')
def waiter_interface(request):
    if request.method == "POST":
        table_id = request.POST.get('table_id')
        if table_id:
            new_order = Order.objects.create(table=Table.objects.get(id=table_id), restaurant=request.user.restaurant, )
            sum = 0
            totalResult = request.POST.getlist("totalResult[]")
            for dish_id in totalResult:
                new_binding = DishOrderBinding.objects.create(order=new_order, dish=Dish.objects.get(id=dish_id),
                                                              restaurant=request.user.restaurant)
                sum += new_binding.dish.price
            new_order.sum = sum
            new_order.save()
            return HttpResponse("200")
        event = request.POST.get("event")
        if event:
            if event == "Item_pressed":
                binding_id = request.POST.get('binding')
                element = DishOrderBinding.objects.get(id=binding_id)
                checkbox_status = request.POST.get("status")
                pay_status = request.POST.get("pay_status")
                if checkbox_status == "true":
                    element.status = '2'
                    element.save()
                    status = True
                    for item in DishOrderBinding.objects.filter(order=element.order):
                        if item.status != "2":
                            status = False
                    if status:
                        element.order.status = "2"
                        element.order.save()
                else:
                    element.status = "1"
                    element.save()
                    if element.order.status == "2":
                        element.order.status = "1"
                        element.order.save()
                print("Order status=",element.order.status)
                print("Dish status =",element.status)
                if checkbox_status == "true" and element.order.status == "2" and element.order.payed == 1:
                    return HttpResponse("Add button")
                else:
                    return HttpResponse("Delete button")
            elif event == "Pay_button_pressed":
                order_id = request.POST.get("order")
                order = Order.objects.get(id=order_id)
                pay_status = request.POST.get("pay_status")
                if pay_status == "true":
                    order.payed = True
                    order.save()
                else:
                    order.payed = False
                    order.save()
                print(order.payed)
                if pay_status == "true":
                    all_items_status = True
                    for item in DishOrderBinding.objects.filter(order=order):
                        if item.status != "2":
                            all_items_status = False
                    if all_items_status:
                        order.status = "2"
                        order.save()
                else:
                    order.status = "1"
                    order.save()
                print("order status =",order.status)
                if pay_status == "true" and order.status == "2" and order.payed == 1:
                    return HttpResponse("Add button")
                else:
                    return HttpResponse("Delete button")

            elif event == "Delete_button_pressed":
                order_id = request.POST.get("order")
                order = Order.objects.get(id = order_id)
                order.status = "3"
                order.save()
                print("order status=", order.status)
                return HttpResponse(200)
            elif event == "Group_pressed":
                group_id = request.POST.get("dish_group_id")
                group = DishGroup.objects.get(id = group_id)
                dishes = Dish.objects.filter(dish_group = group)
                json_answer = []
                for dish in dishes:
                    json_answer.append({"dish_name":dish.name,"id":dish.id})
                print(json_answer)
                return JsonResponse({'dishes':json_answer})
    else:
        tables = Table.objects.filter(restaurant=request.user.restaurant)
        all_orders = {}
        for table in tables:
            orders_per_table = []
            orders = Order.objects.filter(table=table).exclude(status="3")
            for item in orders:
                bindings = DishOrderBinding.objects.filter(restaurant=request.user.restaurant, order=item)
                print(bindings)
                orders_per_table.append({})
                orders_per_table[-1]["bindings"] = bindings
                orders_per_table[-1]['id'] = item.id
                orders_per_table[-1]["sum"] = item.sum
                orders_per_table[-1]["payed"] = item.payed
                print("status",item.status)
            if orders_per_table:
                all_orders[table] = orders_per_table

        tables = Table.objects.filter(restaurant=request.user.restaurant)
        groups = DishGroup.objects.filter(restaurant=request.user.restaurant)

        return render(request, 'waiter.html', {'orders': all_orders,
                                               'restaurant': request.user.restaurant,
                                               "tables": tables, "groups": groups
                                               })

@login_required
@user_passes_test(lambda user: user.type == '2')
def cook(request):
    if request.method == "POST":
        binding_id = request.POST.get('binding')
        element = DishOrderBinding.objects.get(id=binding_id)
        element.status = '1'
        element.save()
        bindings = DishOrderBinding.objects.filter(order=element.order)
        status = True
        for item in bindings:
            if item.status == '0':
                status = False
        if status:
            element.order.status = '1'
            element.order.save()
            return HttpResponse("Delete order")
        else:
            return HttpResponse("Delete dish")
    else:
        tables = Table.objects.filter(restaurant=request.user.restaurant)
        all_orders = {}
        for table in tables:
            orders_per_table = []
            orders = Order.objects.filter(table=table, status="0")
            for item in orders:
                bindings = DishOrderBinding.objects.filter(restaurant=request.user.restaurant, order=item, status="0")
                orders_per_table.append({})
                orders_per_table[-1]["bindings"] = bindings
                orders_per_table[-1]['id'] = item.id
            if orders_per_table:
                all_orders[table] = orders_per_table
        print(all_orders)
        return render(request, 'Cook.html', {"orders": all_orders})
