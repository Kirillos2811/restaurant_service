from django.contrib import admin

from orders.models import Table, Order, DishOrderBinding

admin.site.register(Table)
admin.site.register(Order)
admin.site.register(DishOrderBinding)
# Register your models here.
