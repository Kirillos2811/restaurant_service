# coding: utf-8
from django.db import models

from dishes.models import Dish
from restaraunts.models import Restaurant


class Table(models.Model):
    number = models.IntegerField()
    persons = models.IntegerField(null=True, default=2)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.number) + " table"


class Order(models.Model):
    CHOICES = (
        ('0', 'Ожидает выполнения'),
        ('1', 'Приготовлен'),
        ('2', 'Доставлен'),
        ('3', 'Завершен')
    )
    table = models.ForeignKey(Table, on_delete=models.CASCADE, null=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=1, choices=CHOICES, default='0')
    sum = models.IntegerField(default=0)
    payed = models.BooleanField(default = False)

class DishOrderBinding(models.Model):
    DISH_STATUSES = (
        ('0', 'Ожидает приготовления'),
        ('1', 'Приготовлено'),
        ('2', 'Доставлено')
    )
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE, null=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=1, choices=DISH_STATUSES, default='0')
    carry_immediatelly = models.BooleanField(default=False)
