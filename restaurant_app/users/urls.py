from django.urls import path, include

from . import views

urlpatterns = [
    path('signup/', views.signup,name = 'Registration'),
    path('accounts/admin', views.admin_interface, name='admin_interface'),
    path('', include('django.contrib.auth.urls')),
    path('accounts/',views.accounts,name = "accounts")
]