# Generated by Django 2.2.4 on 2019-11-02 14:13

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_remove_user_expiration_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='expiration_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 11, 2, 17, 14, 22, 570130), null=True),
        ),
    ]
