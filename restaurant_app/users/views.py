from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

from .forms import UserForm, RestaurantForm, AdminForm, DishForm,Dish_group_form


@login_required
@user_passes_test(lambda user: user.type == '0')
def admin_interface(request):
    if request.method == 'POST':
        if "add_dish_button" in request.POST:
            dish_form = DishForm(request.POST)
            user_form = UserForm()
            if dish_form.is_valid():
                new_dish = dish_form.save()
                new_dish.restaurant = request.user.restaurant
                new_dish.save()
                dish_form = DishForm()

        elif "add_user_button" in request.POST:
            user_form = UserForm(request.POST)
            dish_form = DishForm()
            if user_form.is_valid():
                user = user_form.save()
                user.restaurant = request.user.restaurant
                user.save()
                user_form = UserForm()
        #elif "add_dish_group_button" in
    else:
        user_form = UserForm()
        dish_form = DishForm()
        dish_group_form = Dish_group_form()
    return render(request, 'admin.html', {"user_form": user_form,
                                          "dish_form": dish_form,
                                          "dish_group_form":dish_group_form
                                          })


def signup(request):
    if request.method == 'POST':
        user_form = AdminForm(request.POST)
        rest_form = RestaurantForm(request.POST)
        if rest_form.is_valid():
            restaurant = rest_form.save()
            if user_form.is_valid():
                user = user_form.save()
                print(user)
                username = user_form.cleaned_data.get('username')
                raw_password = user_form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)
                print(user)
                login(request, user)
                # restaurant.user_set.add(user,bulk = False)
                user.restaurant = restaurant
                user.save()
                print(user.restaurant)
                return redirect('home')
    else:
        user_form = AdminForm()
        rest_form = RestaurantForm()
    return render(request, 'signup.html', {'user_form': user_form, 'rest_form': rest_form})


def accounts(request):
    if request.user.type == "0":
        return redirect('admin_interface')
    if request.user.type == "1":
        return redirect('waiter_interface')
    if request.user.type == "2":
        return redirect("cook")
