from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime
from restaraunts.models import Restaurant


class User(AbstractUser):
    PERSONAL_CHOICES = (
        ('0', 'admin'),
        ('1', 'Waiter'),
        ('2', 'Cook'),
        ('3', 'Superuser'),
    )
    type = models.CharField(max_length=1, choices=PERSONAL_CHOICES, default='0')
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True, blank=True)
    expiration_date = models.DateField(null=True, blank=True, default=datetime.date.today()+datetime.timedelta(days = 30))


