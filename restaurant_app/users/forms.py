from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User
from .models import Restaurant
from dishes.models import Dish, DishGroup


class Dish_group_form(forms.ModelForm):
    class Meta:
        model = DishGroup
        exclude = ()


class AdminForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username',)


class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'type',)


class RestaurantForm(forms.ModelForm):
    class Meta:
        model = Restaurant
        exclude = ()


class DishForm(forms.ModelForm):
    class Meta:
        model = Dish
        exclude = ('restaurant',)
