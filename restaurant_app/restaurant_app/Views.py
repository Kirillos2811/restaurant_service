from django.shortcuts import render

from orders.models import Order
from restaraunts.models import Restaurant
from users.models import User


def index(request):
    count_orders = len(Order.objects.all())
    return render(request, 'Home2.html', {'count_orders': count_orders
                                         })
