function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

$(".item").click(function() {
    let element = $(this);
    console.log(element.parent().children());
    $.ajax({
        type: 'POST',
        url: "/accounts/waiter/",
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            binding: element.attr('id'),
            status: element.prop("checked"),
            event: "Item_pressed",
            pay_status: element.parent().parent().children(".pay_status").prop("checked")
        },
        success: function(data) {
            if (data == "Add button") {
                element.parent().parent().append($("<button class=del_button >Закрыть заказ</button>"));
            }
            if (data == "Delete button") {
                element.parent().parent().children(".del_button").hide("slow");
            }
        }
    });
});


$(".pay_status").click(function() {
    let element = $(this);
    $.ajax({
      type: 'POST',
      url: "/accounts/waiter/",
      data: { csrfmiddlewaretoken: getCookie('csrftoken'),
              event:"Pay_button_pressed",
              pay_status:element.prop("checked"),
              order:element.parent().attr("id")
            },
      success: function(data){
         if(data == "Add button"){
            element.parent().append($("<button class=del_button >Закрыть заказ</button>"));
        }if(data == "Delete button"){
            element.parent().children(".del_button").hide("slow");
      }
    }});
});


$("body").on("click",".del_button",function(){
    let element = $(this);
    element.parent().hide("slow");
    $.post("/accounts/waiter/",{csrfmiddlewaretoken: getCookie('csrftoken'),"event":"Delete_button_pressed","order":element.parent().attr("id")});
});

let totalResult = [];

$(".group").click(function() {
    $.ajax({
        type: 'POST',
        url: "/accounts/waiter/",
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            dish_group_id:$(this).attr('id'),
            event: "Group_pressed",
        },
        success: function(data) {
            $("#dishes").empty();
            $.each(data.dishes,function(index,value){
                $("#dishes").append($("<li id = " + value.id + " class = dish>" + value.dish_name + "</li>"));
            });
        }
    });
});


$("body").on("click",".dish",function() {
    $("#totalResult").append($("<li id = " + $(this).attr('id') + ">" + $(this).text() + "<img class = del_img src = https://im0-tub-ru.yandex.net/i?id=73d295a617faab0553e26489fee6466f-l&n=13 width = 30 height = 30 style = margin-left:300px>" + "</li>"));
    totalResult.push($(this).attr("id"));
});

$("body").on("click",".del_img",function(){
    let parent_element = $(this).parent();
    let index = totalResult.indexOf(parent_element.attr("id"));
    totalResult.splice(index,1);
    parent_element.hide("slow");
});

$("#sendButton").click(function() {
    if(totalResult.length != 0){
        let table_id = $("select").find('option:selected').attr("id");
        $.post("/accounts/waiter/", { 'totalResult[]': totalResult, csrfmiddlewaretoken: getCookie('csrftoken'), table_id: table_id });
        $("#totalResult").empty();

        $("body").append($("<div class=alert-success style=font-size:40px;left:0;bottom:0;position:fixed;>Заказ успешно создан </div>"));

        setTimeout(function(){
            console.log($(".alert-success"));
            $(".alert-success").hide("slow");
        },1000);

    }
});