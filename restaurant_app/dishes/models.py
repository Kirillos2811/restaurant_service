from django.db import models
from restaraunts.models import Restaurant


class DishGroup(models.Model):
    name = models.CharField(max_length=50)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True)
    def __str__(self):
        return self.name


class Dish(models.Model):
    name = models.CharField(max_length = 50)
    price = models.IntegerField()
    dish_group = models.ForeignKey(DishGroup, on_delete=models.CASCADE, null=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name
