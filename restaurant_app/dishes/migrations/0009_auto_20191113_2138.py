# Generated by Django 2.2.4 on 2019-11-13 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dishes', '0008_auto_20191113_2128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dish',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
