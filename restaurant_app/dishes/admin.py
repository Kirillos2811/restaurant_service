from django.contrib import admin

from dishes.models import Dish,DishGroup

admin.site.register(Dish)
admin.site.register(DishGroup)
# Register your models here.
