import os
import sys
import django
import datetime

PROJECT_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'restaurant_app')
sys.path.append(PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'restaurant_app.settings'
django.setup()


from users.models import User

print(os.environ)

q = User.objects.filter(expiration_date=datetime.date.today(),is_superuser = False).exclude(type = "0")
for user in q:
    print(user)
    user.is_active = False
    user.save()
