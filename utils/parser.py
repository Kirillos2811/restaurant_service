import requests
from random import randrange as rnd,choice
import string
from lxml import html
import datetime
import time
from pprint import pprint
import json
import os
import sys
import django
import datetime

PROJECT_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'restaurant_app')
sys.path.append(PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'restaurant_app.settings'
django.setup()


from users.models import User
from dishes.models import Dish,DishGroup
from restaraunts.models import Restaurant
from orders.models import Table


def get_password():
    password = "ijgFDHDjhnx872bx8wtet678xkjjhgDFDFiuycbwygfkjvnjhg67872"
    for i in range(rnd(100)):
        index = rnd(1,len(password))
        password = password[index:] + password[:index]
    return password


def add_food(food_url):
    food_request = requests.get(food_url)
    food_tree = html.fromstring(food_request.text)
    food_names = food_tree.xpath('//div[@class="position_title"]/text()')
    for i in range(len(food_names)):
        new_string = food_names[i].split("\n")[1].split("                                                            ")[1]
        food_names[i] = new_string


    for dish_name in food_names:
        dish = {}
        dish["name"] = dish_name
        dish["price"] = rnd(0, 3000)
        food.append(dish)


def create_restaurant(restaurants,persons,food_groups,food):
    restaurant = choice(restaurants)
    new_restaraunt = Restaurant.objects.create(name = restaurant["name"],address = restaurant["address"])
    print(restaurant["name"])
    restaurants.pop(restaurants.index(restaurant))
    admin = choice(persons)
    admin["type"] = "0"
    persons.pop(persons.index(admin))

    waiter = choice(persons)
    waiter["type"] = "1"
    persons.pop(persons.index(waiter))

    cook = choice(persons)
    cook["type"] = "2"
    persons.pop(persons.index(cook))
    try:
        User.objects.create(username = admin["name"],
                            password = admin["password"],type = admin["type"],restaurant = new_restaraunt)

        User.objects.create(username=waiter["name"], password=waiter["password"], type=waiter["type"],
                            restaurant=new_restaraunt)

        User.objects.create(username=cook["name"], password=cook["password"], type=cook["type"],
                            restaurant=new_restaraunt)
    except Exception:
        rnd_name_suffix = str(rnd(10)) + str(rnd(10)) + str(rnd(10))
        User.objects.create(username=admin["name"] + rnd_name_suffix,password=admin["password"], type=admin["type"],
                            restaurant=new_restaraunt)

        User.objects.create(username=waiter["name"] + rnd_name_suffix, password=waiter["password"], type=waiter["type"],
                            restaurant=new_restaraunt)

        User.objects.create(username=cook["name"] + rnd_name_suffix, password=cook["password"], type=cook["type"],
                            restaurant=new_restaraunt)

    for i in range(rnd(1,11)):
        try:
            new_user = choice(persons)
            persons.pop(persons.index(new_user))
            User.objects.create(username=new_user["name"], password=new_user["password"], type=new_user["type"],
                                restaurant=new_restaraunt)
        except Exception:
            rnd_name_suffix = str(rnd(10)) + str(rnd(10)) + str(rnd(10))
            User.objects.create(username=new_user["name"] + rnd_name_suffix , password=new_user["password"], type=new_user["type"],
                                restaurant=new_restaraunt)

    for i in range(rnd(1,11)):
        new_group = DishGroup.objects.create(name = food_groups[i],restaurant = new_restaraunt)

        for i in range(rnd(1,11)):
            new_dish = choice(food)
            food.pop(food.index(new_dish))

            Dish.objects.create(name=new_dish["name"],price=new_dish["price"],
                        dish_group = new_group,restaurant = new_restaraunt)

    for i in range(1,rnd(1,30)):
        Table.objects.create(number = i,persons = rnd(1,6),restaurant = new_restaraunt)


def sort_query(names_query):
    to_drop = []
    for i in range(len(names_query)):
        if names_query[i] == "" or names_query[i] == "\xa0":
            to_drop.append(names_query[i])

    for item in to_drop:
        names_query.pop(names_query.index(item))

url = 'https://2gis.ru/search/%D0%9F%D0%BE%D0%B5%D1%81%D1%82%D1%8C?m=38.033267%2C55.873497%2F8.29'

headers = {
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'no-cache',
    'cookie': 'dg5_flags=; dscnt=1; _2gis_webapi_session=0e8cd829-c957-4783-93c2-bd2606efec58; _2gis_webapi_user=98e706b1-f117-4f92-a8df-7e302adc72b6; openBeta=1; _ga=GA1.2.1344675794.1573300740; _gid=GA1.2.1892848997.1573300740; _ym_uid=1567078108571889202; _ym_d=1573300740; _ym_visorc_837864=b; _ym_isad=2; __gads=ID=b655fd4f9467ec50:T=1573300741:S=ALNI_Mb8Qu1Gg4uZ513SJy0zUtIx95GaFA; _2gis_webapi_session=9044d970-8617-4fa6-b953-defd90ce2e1d; _2gis_webapi_user=02c4e96f-263a-4c5d-9997-9ec71d1185c4; dg5_pos=20.473982%3B54.704791%3B11.54; ipp_key=v1573301448963%2Fv3394bd5e96dd894cac2844163aeca6afa04ab3%2FG7m%2bPATG%2bNGH%2f62q6u7COQ%3d%3d; ipp_uid=1573301448962%2fLtwoJVQ6N3XJQWHx%2fuA0mosmr8gj8QAI4KXmhlA%3d%3d; ipp_uid1=1573301448962; ipp_uid2=LtwoJVQ6N3XJQWHx%2fuA0mosmr8gj8QAI4KXmhlA%3d%3d; ipp_uid_tst=; ipp_static_key=',
    'pragma': 'no-cache',
    'Referer': 'https://2gis.ru/moscow/search/%D0%9F%D0%BE%D0%B5%D1%81%D1%82%D1%8C?m=37.504144%2C55.894654%2F10.68',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36'
}

count = 1
restaurants = []
while True:
    try:
        r = requests.get(url, headers=headers)
        tree = html.fromstring(r.text)

        names_list = tree.xpath('//span[@class="_hc69qa"]/text()')
        address_list = tree.xpath('//span[@class="_tluih8"]/text()')

        for i in range(len(names_list)):
            rest_data = {}
            rest_data["name"] = names_list[i]
            rest_data["address"] = address_list[i]
            restaurants.append(rest_data)

        count += 1
        print(count)
        url = "https://2gis.ru/search/%D0%9F%D0%BE%D0%B5%D1%81%D1%82%D1%8C/page/" + str(count) + "?m=38.033267%2C55.873497%2F8.29"
        if count == 10:
            break
    except Exception:
        break

food_urls = ["https://kupidonia.ru/spisok/spisok-bljud-japonskoj-kuhni","https://kupidonia.ru/spisok/spisok-bljud-russkoj-kuhni-nazvanija-po-alfavitu","https://kupidonia.ru/spisok/spisok-bljud-kitajskoj-kuhni"]

groups_url = "https://tvoi-povarenok.ru/recipes"
groups_request = requests.get(groups_url)
groups_tree = html.fromstring(groups_request.text)
food_groups = groups_tree.xpath('//p[@class="postsbox-title"]/a/text()')


food = []

for item in food_urls:
    add_food(item)

users = []
names_url = "http://www.english-source.ru/english-linguistics/english-lexis/148-english-names"
names_request = requests.get(names_url)
names_tree = html.fromstring(names_request.text)
names_query = list(map(lambda item:item.split("\r\n\t\t\t\t")[1],names_tree.xpath("//td/text()")[3:]))

sort_query(names_query)

persons = []

for item in names_query:
    new_person = {}
    new_person["name"] = item
    new_person["password"] = get_password()
    new_person["type"] = str(rnd(3))
    persons.append(new_person)

print(restaurants)
print(persons)
print(food_groups)
print(food)

for i in range(10):
    create_restaurant(restaurants,persons[::],food_groups[::],food[::])